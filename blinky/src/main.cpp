/**
 * Blink mm
 *
 * Turns on an LED on for one second,
 * then off for one second, repeatedly.
 */
#include "Arduino.h"

#if defined ARDUINO_XIAO_ESP32C3
#define LED D10
#elif defined ARDUINO_AVR_UNO
#define LED 13
#endif

void setup()
{
  // initialize LED digital pin as an output.
  pinMode(LED, OUTPUT);
}

void loop()
{
  // flash quickly 2 twice
  for(int i=0; i<4; i++) {
    digitalWrite(LED, HIGH);
    delay(200);
    digitalWrite(LED, LOW);
    delay(100);
  }
  delay(800);
  for(int i=0; i<4; i++) {
    digitalWrite(LED, HIGH);
    delay(200);
    digitalWrite(LED, LOW);
    delay(100);
  }
  delay(1000);
}
